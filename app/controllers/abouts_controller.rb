class AboutsController < ApplicationController

	before_action :is_editor_or_admin, only: [:new, :edit, :create, :update, :destroy]

    def new
        @about = About.new
    end
    
    def edit
        @about = About.find(params[:id])
        if(@about.author != current_user.name)
        	redirect_to abouts_url
        end
    end
    
    def create
        @about = About.new(about_params)
        @about.author = current_user.name;
        if @about.save
            redirect_to abouts_url
        else
            render 'new'
        end
    end
    
    def update
        @about = About.find(params[:id])
        if @about.update(about_params)
            redirect_to abouts_url
        else 
            render 'edit'
        end
    end
        
    def show
        @about = About.find(params[:id])
    end
    
    def index
        @abouts = About.all
    end
    
    def destroy
        @about = About.find(params[:id])
        if @about.author == current_user.name || current_user.user_role == "Admin"
        	@about.destroy
        end
        redirect_to abouts_path
    end
    
    private
        def about_params
            params.require(:about).permit(:title, :author, :text)
        end
        
         def is_editor_or_admin
        	redirect_to(Members_url) unless (current_user.user_role == "Editor" || current_user.user_role == "Admin")
        end
    end
