class EventsController < ApplicationController

	before_action :is_editor_or_admin, only: [:new, :edit, :create, :update, :destroy]

    def new
        @event = Event.new
    end
    
    def edit
        @event = Event.find(params[:id])
        if(@event.author != current_user.name)
        	redirect_to events_url
        end
    end
    
    def create
        @event = Event.new(event_params)
        @event.author = current_user.name;
        if @event.save
            redirect_to events_url
        else
            render 'new'
        end
    end
    
    def update
        @event = Event.find(params[:id])
        if @event.update(event_params)
            redirect_to events_url
        else 
            render 'edit'
        end
    end
        
    def show
        @event = Event.find(params[:id])
    end
    
    def index
        @events = Event.all
    end
    
    def destroy
        @event = Event.find(params[:id])
        if @event.author == current_user.name || current_user.user_role == "Admin"
        	@event.destroy
        end
        redirect_to events_url
    end
    
    private
        def event_params
            params.require(:event).permit(:title, :author, :text)
        end
        
        def is_editor_or_admin
        	redirect_to(events_url) unless (current_user.user_role == "Editor" || current_user.user_role == "Admin")
        end
    end
