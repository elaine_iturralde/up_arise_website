class MembersController < ApplicationController

	before_action :is_editor_or_admin, only: [:new, :edit, :create, :update, :destroy]

    def new
        @member = Member.new
    end
    
    def edit
        @member = Member.find(params[:id])
        if(@member.author != current_user.name)
        	redirect_to members_url
        end
    end
    
    def create
        @member = Member.new(member_params)
        @member.author = current_user.name;
        if @member.save
            redirect_to members_url
        else
            render 'new'
        end
    end
    
    def update
        @member = Member.find(params[:id])
        if @member.update(member_params)
            redirect_to members_url
        else 
            render 'edit'
        end
    end
        
    def show
        @member = Member.find(params[:id])
    end
    
    def index
        @members = Member.all
    end
    
    def destroy
        @member = Member.find(params[:id])
        if @member.author == current_user.name || current_user.user_role == "Admin"
        	@member.destroy
        end
        redirect_to members_url
    end
    
    private
        def member_params
            params.require(:member).permit(:title, :author, :text)
        end
        
        def is_editor_or_admin
        	redirect_to(members_url) unless (current_user.user_role == "Editor" || current_user.user_role == "Admin")
        end
    end
