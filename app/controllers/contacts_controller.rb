class ContactsController < ApplicationController

	before_action :is_editor_or_admin, only: [:new, :edit, :create, :update, :destroy]

    def new
        @contact = Contact.new
    end
    
    def edit
        @contact = Contact.find(params[:id])
        if(@contact.author != current_user.name)
        	redirect_to contacts_url
        end
    end
    
    def create
        @contact = Contact.new(contact_params)
        @contact.author = current_user.name;
        if @contact.save
            redirect_to contacts_url
        else
            render 'new'
        end
    end
    
    def update
        @contact = Contact.find(params[:id])
        if @contact.update(contact_params)
            redirect_to contacts_url
        else 
            render 'edit'
        end
    end
        
    def show
        @contact = Contact.find(params[:id])
    end
    
    def index
        @contacts = Contact.all
    end
    
    def destroy
        @contact = Contact.find(params[:id])
        if @contact.author == current_user.name || current_user.user_role == "Admin"
        	@contact.destroy
        end
        redirect_to contacts_path
    end
    
    private
        def contact_params
            params.require(:contact).permit(:title, :author, :text)
        end
        
         def is_editor_or_admin
        	redirect_to(Members_url) unless (current_user.user_role == "Editor" || current_user.user_role == "Admin")
        end
    end
