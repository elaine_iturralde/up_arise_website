class ArticlesController < ApplicationController

	before_action :is_editor_or_admin, only: [:new, :edit, :create, :update, :destroy]

    def new
        @article = Article.new
    end
    
    def show
    	@article = Article.find(params[:id])
    end
    
    def edit
        @article = Article.find(params[:id])
         if(@article.author != current_user.name)
        	redirect_to articles_url
        end
    end
    
    def create
        @article = Article.new(article_params)
        @article.author = current_user.name;
        if @article.save
            redirect_to articles_url
        else
            render 'new'
        end
    end
    
    def update
        @article = Article.find(params[:id])
        if @article.update(article_params)
            redirect_to articles_url
        else 
            render 'edit'
        end
    end
    
    def index
        @articles = Article.all
    end
    
    def destroy
        @article = Article.find(params[:id])
        if @article.author == current_user.name || current_user.user_role == "Admin"
        	@article.destroy
        end
        redirect_to articles_path
    end
    
    private
        def article_params
            params.require(:article).permit(:title, :author, :text)
        end
        
        def is_editor_or_admin
        	redirect_to(Members_url) unless (current_user.user_role == "Editor" || current_user.user_role == "Admin")
        end
end
