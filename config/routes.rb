Rails.application.routes.draw do

  resources :uploads
  resources :abouts
  resources :members
  resources :events
  resources :contacts

  root 					'up_arise#Home'
  get 'signup'		=>	'users#new'
  put 'role'		=>  'users#update'
  get 'login'		=>	'sessions#new'
  post 'login'		=>	'sessions#create'
  delete 'logout'	=>	'sessions#destroy'
  
  resources :users 
  resources :articles

end
